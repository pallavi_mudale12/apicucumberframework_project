Feature: Trigger post API 
@post
Scenario: TRigger the API request with valid request parameters
       Given Enter NAME and JOB in request body
       When Send the request with payload
       Then Validate status code 
       And Validate response body parameter
 @Put      
Scenario: TRigger the Put API request with valid request parameters
       Given Input NAME and JOB in put request body
       When Send the put request with put payload
       Then Validate put status  code 
       And Validate put response body parameter  
 @Patch      
Scenario: TRigger  Patch API request with valid request parameters
       Given Enter NAME and JOB in patch request body
       When Send the patch request with payload
       Then Validate patch  status code 
       And Validate patch response body parameter     
 @Get      
Scenario: TRigger the Get API request with valid request parameters
       Given valid Get request Base Url
       When Send the get request
       Then Validate Get status code 
       And Validate get response body parameter              

       