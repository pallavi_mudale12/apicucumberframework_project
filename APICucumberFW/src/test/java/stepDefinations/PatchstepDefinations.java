package stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import API_common_methods.common_method_handle_api;
import Endpoint.Patch_endpoint;
import Request_repository.Patch_request_repository;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class PatchstepDefinations {
	String requestBody;
	int statusCode;
	String responseBody;
	String endpoint;
	File log_dir;
	
	@Given("Enter NAME and JOB in patch request body")
	public void enter_name_and_job_in_patch_request_body() throws IOException {
		log_dir=Handle_directory.create_log_directory("patch_tc1_logs");
		 requestBody = Patch_request_repository.patch_request_tc1();
		 endpoint = Patch_endpoint.patch_endpoint_tc1();
	    }
	
	@When("Send the patch request with payload")
	public void send_the_patch_request_with_payload() throws IOException {
		statusCode = common_method_handle_api.patch_statusCode(requestBody, endpoint);
		responseBody = common_method_handle_api.patch_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
		Handle_api_logs.evidence_creator(log_dir,"patch_tc1",endpoint,requestBody,responseBody);
	    
	    
	}
	@Then("Validate patch  status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(statusCode, 200);
	    
	}
	@Then("Validate patch response body parameter")
	public void validate_patch_response_body_parameter() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdat = jsp_res.getString("updatedAt");
		res_createdat = res_createdat.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdat, expecteddate);
		System.out.println("Patch API Response Body Validation Successfull");
	   
	    
	}


}
