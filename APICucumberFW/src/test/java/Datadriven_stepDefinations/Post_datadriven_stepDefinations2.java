package Datadriven_stepDefinations;
import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Post_datadriven_stepDefinations2 {
	String requestBody;
	int statuscode;
	String responseBody;
	
	@Given("Enter {string} and {string} in request body")
	public void enter_and_in_request_body(String req_name, String req_job) {
		String BaseURI = "https://reqres.in/";
		RestAssured.baseURI = BaseURI;
		requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
	    
	    //throw new io.cucumber.java.PendingException();
	}

	@When("Send the request with data payload")
	public void send_the_request_with_payload() {
		statuscode = given().header("Content-Type", "application/json").body(requestBody).when().post("/api/users")
				.then().extract().statusCode();
		responseBody = given().header("Content-Type", "application/json").body(requestBody).when()
				.post("/api/users").then().extract().response().asString();
		System.out.println(responseBody);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate post_datadriven status code")
	public void validate_post_datadriven_status_code() {
		Assert.assertEquals(statuscode, 201);
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("Validate post_datadriven response body parameter")
	public void validate_response_body_parameter() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		JsonPath jspresponse = new JsonPath(responseBody);
		String res_name=jspresponse.getString("name");
		String res_job=jspresponse.getString("job");
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		System.out.println("Response Body Validation Successfull");
	    //throw new io.cucumber.java.PendingException();
	}



}
