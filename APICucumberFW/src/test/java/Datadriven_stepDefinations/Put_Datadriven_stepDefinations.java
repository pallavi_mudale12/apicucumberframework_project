package Datadriven_stepDefinations;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import API_common_methods.common_method_handle_api;
import Endpoint.Put_endpoint;
import Request_repository.Put_request_repository;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

public class Put_Datadriven_stepDefinations {
	String requestBody;
	int statusCode;
	String responseBody;
	File log_dir;
	String endpoint;
	
	@Given("Enter {string} and {string} in Put requestbody")
	public void enter_and_in_put_requestbody(String req_name, String req_job) throws IOException {
		log_dir=Handle_directory.create_log_directory("put_tc1_logs");
		endpoint = Put_endpoint.put_endpoint_tc1();
		requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
	    
	}
	@When("Send the datadriven_Put request  with payload")
	public void send_the_datadriven_put_request_with_payload() throws IOException {
		statusCode = common_method_handle_api.put_statusCode(requestBody, endpoint);
		responseBody = common_method_handle_api.put_responseBody(requestBody, endpoint);
		System.out.println(responseBody);
		Handle_api_logs.evidence_creator(log_dir,"put_tc1",endpoint,requestBody,responseBody);
	   
	}
	@Then("Validate datadriven_Put status code")
	public void validate_datadriven_put_status_code() {
		Assert.assertEquals(statusCode, 200);
	   
	}
	@Then("Validate datadriven_Put response body parameter")
	public void validate_datadriven_put_response_body_parameter() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		JsonPath jsp_res = new JsonPath(responseBody);
		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_createdat = jsp_res.getString("updatedAt");
		res_createdat = res_createdat.substring(0, 11);

	    Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdat, expecteddate);
		System.out.println(" Put Response Body Validation Successfull");
	    
	}



}
