Feature: Trigger Patch API 

Scenario Outline: TRigger the Patch API request with valid  parameters
       Given Enter "<Name>" and "<Job>" in Patch requestbody
       When Send the datadriven_Patch request  with payload
       Then Validate datadriven_Patch status code 
       And Validate datadriven_Patch response body parameter

Examples:
        |Name|Job|
        |Tungesh|CEO|
        |Tamohara|Egg|
        |Srikanth|Qadev|