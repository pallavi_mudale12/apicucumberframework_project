Feature: Trigger Put API 

Scenario Outline: TRigger the Put API request with valid  parameters
       Given Enter "<Name>" and "<Job>" in Put requestbody
       When Send the datadriven_Put request  with payload
       Then Validate datadriven_Put status code 
       And Validate datadriven_Put response body parameter

Examples:
        |Name|Job|
        |Neha|Mgr|
        |Viju|Tl|
        |Pallavi|Hr|