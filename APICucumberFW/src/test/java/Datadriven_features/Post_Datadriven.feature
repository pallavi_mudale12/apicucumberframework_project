Feature: Trigger post API 

Scenario Outline: TRigger the API request with valid request parameters
       Given Enter "<Name>" and "<Job>" in request body
       When Send the request with data payload
       Then Validate post_datadriven status code 
       And Validate post_datadriven response body parameter

Examples:
        |Name|Job|
        |Anushka|Mgr|
        |Anu|QA|
        |Putti|QaLead|