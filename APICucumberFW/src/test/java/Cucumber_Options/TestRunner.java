package Cucumber_Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith(Cucumber.class)

@CucumberOptions(features="src/test/java/features", glue= {"stepDefinations"} , tags="@post or @Put or @Patch")
public class TestRunner {

}
